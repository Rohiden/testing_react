import React, { Component } from 'react';

class App extends Component {
  state = {
    count: 0
  }

  render() {
    return (
      <div className="App">
        <th>React test app</th>
        <button onClick={this.onClick}>add</button><br />
        <span>{this.state.count}</span>
      </div>
    );
  }

  onClick = () => {
    this.setState((prevState) => {
      return {
        count: prevState.count + 1
      }
    })
  }
}

export default App;

