import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe('App', () => {
    let wrapper;
    beforeEach(() => {
        wrapper = shallow(<App />);

    })
    it('should have the "th" "React test app"', () => {
        expect(wrapper.contains(<th>React test app</th>)).toBe(true);
    })

    it('should have "button"', () => {
        expect(wrapper.containsMatchingElement(
            <button>add</button>
        )).toBe(true);
    })

    describe('button click', () => {
        it('should increase state count', () => {
            const button = wrapper.find('button').first();
            const countOfClicks = 4;

            expect(wrapper.state().count).toBe(0);
            for (let index = 0; index < countOfClicks; index++) {
                button.simulate('click');
            }
            expect(wrapper.state().count).toBe(countOfClicks);
        });
    })
})


